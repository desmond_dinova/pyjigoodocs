import gspread
from oauth2client.service_account import ServiceAccountCredentials


class SpreadSheet:
    def __init__(self, sheet_name: str):
        self.scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        self.credentials = ServiceAccountCredentials.from_json_keyfile_name("client_secret.json", self.scope)
        self.client = gspread.authorize(self.credentials)
        self.sheet_name = sheet_name

        self.TICKET_COLUMN = 14
        self.STATUS_COLUMN = 16

        self.sheet = self.get_sheet()

    def get_sheet(self):
        try:
            return self.client.open(self.sheet_name).sheet1
        except:
            Exception("gspread error")

    def get_tickets_raw(self):
        return self.sheet.col_values(self.TICKET_COLUMN)

    def get_statuses_raw(self):
        return self.sheet.col_values(self.STATUS_COLUMN)
