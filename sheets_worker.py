from pyra import PyRa
from google_sheets import SpreadSheet
import pprint

pp = pprint.PrettyPrinter()


class SheetsWorker:
    def __init__(self, login: str, password: str, sheet_name: str):
        self.jr = PyRa(login, password)
        self.gd = SpreadSheet(sheet_name)

    def set_statuses(self, tickets_amount=500):
        tickets = self.gd.get_tickets_raw()
        statuses = []

        for ticket in tickets:
            if "jira" in ticket:
                status = self.jr.get_status(ticket.split("/")[-1])
                statuses.append(str(status))
            else:
                statuses.append("")

        cell_list = self.gd.sheet.range('P1:P{}'.format(tickets_amount))

        for i in range(len(statuses)):
            cell_list[i].value = statuses[i]

        self.gd.sheet.update_cells(cell_list)
