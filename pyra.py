from jira import JIRA


class PyRa:
    def __init__(self, login: str, password: str, url="https://jira.plamee.com"):
        self.auth = (login, password)
        self.options = {"server": url}

        self.handler = JIRA(self.options, basic_auth=self.auth)

    def get_ticket(self, issue: str):
        try:
            ticket = self.handler.issue(issue)
            return ticket
        except:
            Exception("Something went wrong")

    def get_status(self, ticket: str):
        try:
            status = self.get_ticket(ticket).fields.status
            return status
        except:
            Warning("Cannot access ticket {}".format(ticket))
            pass
